"use strict";

const dynamodb = require("./lib/util/dynamodb-lib");
const response = require("./lib/util/response-lib");

exports.redeemGiftCard = async (event, context, callback) => {
  const data = JSON.parse(event.body);

  let params = {
    TableName: "Giftcard",
    Key: {
      code: data.code
    },
    UpdateExpression: "SET redeemed = :redeem, redeemed_at = :date",
    ConditionExpression: "attribute_not_exists(redeemed_at)",
    ExpressionAttributeValues: {
      ":redeem": 1,
      ":date": Date()
    }
  };

  try {
    await dynamodb.call("update", params);
    return response.success({
      status: true,
      message: "Giftcard Redeemed!"
    });
  } catch (error) {
    console.log(error);
    if (error.code === "ConditionalCheckFailedException") {
      return response.failure({
        status: false,
        message: "Giftcard has already been redeemed!"
      });
    }

    return response.failure({
      status: false,
      message: "Unable to redeem Giftcard. Please try again."
    });
  }
};
