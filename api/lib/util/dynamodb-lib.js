const AWS = require("aws-sdk");

// creates a wrapper function to make a dynamodb call and returns a promise after the call is finished
module.exports.call = call = (action, params) => {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();

  return dynamoDb[action](params).promise();
};
