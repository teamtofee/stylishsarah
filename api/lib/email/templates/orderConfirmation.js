exports.genOrderConf = formContent => {
  const code = `<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>*|MC:SUBJECT|*</title>
    <style type="text/css">
    .background {
        background: linear-gradient(180deg, #F7F3F2 23.75%, #71697B 23.75%);
        font-family: Lato, sans-serif;
    }
    
    a {
        text-decoration: none;
    }
    
    h1, h2 {
        margin: 0;
        font-family: Lato, sans-serif;
        font-weight: normal;
        font-size: 34px;
        line-height: normal;
    }
    
    h3 {
        font-family: Didot, sans-serif;
        color:#666666;
    }
    
    .logo-container {
        margin: 0 auto;
        width: 500px;
    }
    
    .logo-container img {
        text-align: left;
        margin: 0;
    }
    
    .logo-container h3 {
        margin-top: 1px; 
    }
    
    .line-1 {
        border-style: none;
        border-top: 1px solid #BFBFBF;
        margin-left: 0;
        width: 325px;
    }
    
    img {
        display: block;
        margin: 0 auto;
    }
    
    .container {
        background-color: #FFF;
        border-radius: 10px;
        margin: 0 auto;
        padding: 20px;
        
        width: 60%;
    }
    
    .top-container {
        text-align: center;
    }
    
    .top-container h2 {
        margin: 20px auto 20px;
    }
    
    .top-container p {
        font-family: Didot, sans-serif;
        margin: 3px auto 3px;
    }
    
    .middle-container table {
        width: 80%;
        border: 1px solid grey;
        border-radius: 10px;
        margin: 0 auto;
    }
    
    .middle-container table thead {
        text-align: left;
        
    }
    
    .middle-container table {
        padding: 20px;
    }
    
    .bottom-container {
        text-align: center;
    }
    
    .bottom-container p {
        width: 70%;
        margin: 20px auto;
    }
    
    .bottom-div {
        margin: 0 auto;
        width: 60%;
        color: #FFF;
        text-align: center;
    }
    
    .bottom-div img {
        display: inline-block;
        margin: 10px 40px;
    }
    
    .bottom-div p {
        margin: 50px auto 10px auto;
        text-align: center;
        width: 120px;
        font-weight: bold;
        font-size: 24px;
        line-height: normal;
    }
    
    .social {
        margin-bottom: 20px;
    }
    
    .line-2 {
        border-style: none;
        border-top: 1px solid #ECEBEE;
        max-width: 600px;
    }
    
    .links {
        margin: 20px;
    }
    
    .links a {
        margin: 0 8px;
        text-decoration: underline;
    }
    
    .links a:visited {
        color: #FFF;
    }
    
    .desc {
        font-family: Lato;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 31px;
        letter-spacing: 0.930001px;
    }

    </style>


</head>

<body class="background">
    <!-- Logo and Phrase -->
    <div class="logo-container">
        <img src="cid:Logo.png" alt="logo-img">
        <hr class="line-1">
        <h3>Your Life.Styled.</h3>
    </div>

    <!-- Giftcard Details -->
    <div class="container">

        <div class="top-container">
            <h2>Order Confirmation</h2>
            <p><em>Thank you for your purchase!</em></p>
            <p><em>Your gift is on it's way</em></p>
        </div>

        <div class="middle-container">
            <table>
                <thead>
                    <tr>
                        <th>${formContent.date}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Product</td>
                        <td>Amount</td>
                    </tr>
                    <tr><td> ${formContent.item.name}</td><td>$${formContent.item.amount}</td></tr>
                    </tbody>
                <tfoot>
                    <tr>
                        <td>Total Paid</td>
                        <td>$${formContent.total}</td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="bottom-container">
            <p>the giftcard has been sent to ${formContent.recipientName} with instructions on how
                to reedem it. for any questions feel free to contact me at blabk@gmail.com. If recipient hasn't recived
                it, please tell them to check their spam folder.
            </p>
        </div>

    </div>

    <!-- Social and Links -->

    <div class="bottom-div">

        <div class="social">
            <p>Follow Me</p>
            <a href="#">
                <img src="cid:twitter.png" alt="twitter-logo">
            </a>
            <a href="#">
                <img src="cid:facebook.png" alt="facebook-logo">
            </a>
            <a href="#">
                <img src="cid:instagram.png" alt="instagram-logo">
            </a>
            <a href="#">
                <img src="cid:youtube.png" alt="youtube-logo">
            </a>
        </div>

        <hr class="line-2">

        <div class="links">
            <a href="#">
                <span>About</span>
            </a>
            <a href="#">
                <span>Contact</span>
            </a>
            <a href="#">
                <span>Eye Spy</span>
            </a>
            <a href="#">
                <span>The Process</span>
            </a>
            <a href="#">
                <span>Videos</span>
            </a>
            <a href="#">
                <span>Fashion Resume</span>
            </a>
        </div>

        <span class="desc">StylishSarah&trade; | Sarah Sulzberger Perpich | Personal Stylist | Fashion Consultant</span>

    </div>

</body>
</html>`;

  return code;
};
