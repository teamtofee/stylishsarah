const mailgun = require("mailgun-js");
const DOMAIN = "tofeeinc.com";
const api_key = process.env.MAILGUN;
const mg = mailgun({ apiKey: api_key, domain: DOMAIN });
const logo = process.cwd() + "/lib/email/images/Logo.png";
const twitter = process.cwd() + "/lib/email/images/twitter.png";
const facebook = process.cwd() + "/lib/email/images/facebook.png";
const instagram = process.cwd() + "/lib/email/images/instagram.png";
const youtube = process.cwd() + "/lib/email/images/youtube.png";
const { genPurchaseRequest } = require("./templates/requestEmail");
const { genOrderConf } = require("./templates/orderConfirmation");
const { genGiftCard } = require("./templates/recipientConfEmail");

/*
  This library helps create custom email templates, and abstracts the mailgun email API. 
*/

const sendEmail = emailContent => {
  const { from, to, subject, text, html, inline } = emailContent;
  const email = { from, to, subject, text, html, inline };
  return mg.messages().send(email);
};

const sendOrderConf = emailData => {
  const subject = "Order Confirmation";
  const emailContent = {
    date: emailData.date,
    item: emailData.item,
    total: emailData.total,
    recipientName: emailData.recipientName
  };
  const tmpl = genOrderConf(emailContent);

  const emailInfo = {
    from: "StylishSarah <george.nwankwo@tofeeinc.com>",
    to: emailData.to,
    subject: subject,
    html: tmpl,
    inline: [twitter, logo, facebook, instagram, youtube]
  };
  return sendEmail(emailInfo);
};

const sendGiftCard = emailData => {};

const sendPurchaseRequest = emailData => {
  const tmpl = genPurchaseRequest(emailData);
  let subject = "Gift Card Purchase Request";
  var emailInfo = {
    from: "Interested Client <george.nwankwo@tofeeinc.com>",
    to: "qureshihashir@gmail.com",
    subject: subject,
    html: tmpl
  };
  return sendEmail(emailInfo);
};

module.exports = {
  sendOrderConf,
  sendGiftCard,
  sendPurchaseRequest,
  sendEmail
};
