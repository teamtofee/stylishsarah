"use strict";

const dynamodb = require("./lib/util/dynamodb-lib");
const response = require("./lib/util/response-lib");

exports.listAllGiftcards = async (event, context, callback) => {
  let params = {};
  let queryType = "";

  if (event.queryStringParameters && event.queryStringParameters.email) {
    params = {
      TableName: "Giftcard",
      IndexName: "buyer_email-index",
      KeyConditionExpression: "buyer_email = :be",
      ExpressionAttributeValues: {
        ":be": event.queryStringParameters.email
      }
    };

    queryType = "query";
  } else {
    params = {
      TableName: "Giftcard"
    };

    queryType = "scan";
  }

  try {
    const result = await dynamodb.call(queryType, params);
    return response.success(result["Items"]);
  } catch (error) {
    console.log(error);
    return response.failure({
      status: false,
      message: "Unable to retrieve Giftcards. Please try again."
    });
  }
};
