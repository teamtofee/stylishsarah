"use strict";

const email = require("./lib/email/email-lib");

const response = require("./lib/util/response-lib");

exports.purchaseRequest = async (event, context, callback) => {
  const formContent = JSON.parse(event.body);

  try {
    const result = await email.sendPurchaseRequest(formContent);

    return response.success({
      status: true,
      message: result
    });
  } catch (error) {
    return response.failure({
      status: false,
      message: error.stack
    });
  }
};
