# Requirements

When deploying, you need to have the AWS CLI configured with the correct account. You will also need to have the AWS SAM CLI configured. Run `npm install` to install all dependencies before packaging and deploying.

# Packaging

An S3 bucket is needed for this step.
The template.yml file needs to be packaged before being deployed. Use the following command to package the template and put the package in an s3 bucket:

```code
sam package --template-file template.yml --output-template-file packaged-template.yml --s3-bucket NAME_OF_YOUR_S3_BUCKET
```

# Deploying

After packaging, the template is ready to be deployed using the following command:

```code
sam deploy --template-file packaged-template.yml --stack-name NAME_OF_YOUR_STACK --capabilities CAPABILITY_IAM --parameter-overrides MAILGUN=MAILGUN_API_KEY
```

This will deploy the stack in the currently configured region for the account that is deploying the stack (the account in the CLI). This is where you pass the MAILGUN API Key as an environment variable

# Not Working

The /create route only creates the giftcard, it doesn't send the email yet.

# Request Structure

### GET :: /{code}

Nothing is needed in the body, but you need to pass a valid code as a path parameter for a giftcard to get its details

### POST :: /create

Need the following in the body:

```code
{
	"date": "January 30, 2019",
	"name": "Giftcard",
	"item": {"name":"Giftcard", "amount":500},
	"total": 500,
	"recipientName": "John",
	"buyerEmail": "gnwankwo03@gmail.com",
	"recipientEmail": "qureshihashir63@gmail.com"
}
```

### POST :: /contact

Send JSON in the body:

```code
{
	"senderFirstName": "Hashir",
	"senderLastName": "Qureshi",
	"senderEmail": "",
	"senderNumber": "837236923",
	"senderMessage": "I would like to purchase a gift card please!",
	"recipientFirstName": "MEEEE",
	"recipientLastName": "ALSO MEEEE",
	"recipientEmail": "MEEE@MEEE.com",
	"recipientNumber": "980707"
}

```

### GET :: /getGiftcards

If no query parameters are passed in, will return all giftcards. If `?email` is passed in, will return all giftcards with the buyer email equalling the passed in email.

### PATCH :: /redeem

Needs the following in the body:

```code
{
        "code": "2ewc312cw312wd"

}
```
