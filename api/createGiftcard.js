"use strict";

const dynamodb = require("./lib/util/dynamodb-lib");
const response = require("./lib/util/response-lib");
const uniqid = require("uniqid");
// const email = require("./lib/email/email-lib");

exports.createGiftcard = async (event, context, callback) => {
  const data = JSON.parse(event.body);

  const code = uniqid();
  let params = {
    TableName: "Giftcard",
    KeyConditionExpression: "code = :c",
    ExpressionAttributeValues: {
      ":c": code
    }
  };

  // const result = await email.sendOrderConf({ ...data, to: data.buyerEmail });

  // return response.success({
  //   status: true,
  //   message: result
  // });

  try {
    const result = await dynamodb.call("query", params);
    if (result["Items"] && result["Items"].length !== 0) {
      // Giftcard with code exists. return with failure
      console.log("Giftcard with code already exists.");
      return response.failure({
        status: false,
        message: "Unable to create giftcard please try again"
      });
    } else {
      //Giftcard with code doesn't exist. attempt to put new card in table.
      params = {
        TableName: "Giftcard",
        Item: {
          code: code,
          buyer_email: data.buyerEmail,
          recipient_email: data.recipientEmail,
          amount: parseFloat(data.item.amount),
          created_at: Date()
        }
      };
      await dynamodb.call("put", params);
      return response.success(params["Item"]);
    }
  } catch (error) {
    console.log(error);
    return response.failure({
      status: false,
      message: "Unable to create giftcard please try again"
    });
  }
};
