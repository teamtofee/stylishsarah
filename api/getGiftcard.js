"use strict";

const dynamodb = require("./lib/util/dynamodb-lib");
const response = require("./lib/util/response-lib");

exports.getGiftcard = async (event, context, callback) => {
  let params = {
    TableName: "Giftcard",
    KeyConditionExpression: "code = :c",
    ExpressionAttributeValues: {
      ":c": event.pathParameters.code
    }
  };

  try {
    const result = await dynamodb.call("query", params);
    return response.success(result["Items"]);
  } catch (error) {
    console.log(error);
    return response.failure({
      status: false,
      message: "Unable to retrieve Giftcard. Please try again."
    });
  }
};
