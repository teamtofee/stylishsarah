import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import { Route, BrowserRouter } from "react-router-dom";
import LandingPage from "./components/LandingPage";
import Header from "./components/Header";
import About from "./components/About";
import Footer from "./components/Footer";

const App = () => (
  <BrowserRouter>
    <div className="app">
      <Header />
      <Route exact path="/" component={LandingPage} />
      <Route exact path="/about" component={About} />
      <Footer />
    </div>
  </BrowserRouter>
);

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
