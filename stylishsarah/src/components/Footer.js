import React, { Component } from "react";
import { Link } from "react-router-dom";
import styles from "../styles/Footer.js";

class Footer extends Component {
  render() {
    return (
      <div>
        <footer style={styles.footer}>
          <ul style={styles.container}>
            <li>
              <img
                src={require("../logo_white.svg")}
                style={styles.stylishSarah}
                alt="StylishSarah"
              />
              <div style={styles.line1} />
            </li>
            <li />
            <li>
              <Link to="/" style={styles.theProcess}>
                The Process
              </Link>
            </li>
            <li>
              <Link to="/About" style={styles.about}>
                About
              </Link>
            </li>
            <li>
              <Link to="/Eyespy" style={styles.eyespy}>
                Eye Spy
              </Link>
            </li>
          </ul>
          <ul style={styles.container}>
            <li>
              <div style={styles.work}>WORK</div>
              <div style={styles.line2} />
              <Link to="/Fashion" style={styles.fashionResume}>
                Fashion Resume
              </Link>
            </li>
            <li>
              <Link to="/Videos" style={styles.videos}>
                Videos
              </Link>
            </li>
            <li>
              <Link to="/Contact" style={styles.contact}>
                Contact
              </Link>
            </li>
            <li>
              <Link to="/GiftCards" style={styles.giftcard}>
                GiftCard
              </Link>
            </li>
          </ul>
          <ul style={styles.container}>
            <li>
              <span style={styles.followMe}>FOLLOW ME</span>
              <div style={styles.line3} />
            </li>
            <li>
              <Link to="/Facebook" style={styles.facebook}>
                Facebook
              </Link>
            </li>
            <li>
              <Link to="/Twitter" style={styles.twitter}>
                Twitter
              </Link>
            </li>
            <li>
              <Link to="/YouTube" style={styles.youtube}>
                You Tube
              </Link>
            </li>
            <li>
              <Link to="/Instagram" style={styles.instagram}>
                Instagram
              </Link>
            </li>
          </ul>
          <div style={styles.stylishSarahSara}>
            StylishSarah&trade; | Sarah Sulzburger Perpich | Personal Stylist |
            Fashion Consultant
          </div>
        </footer>
      </div>
    );
  }
}

export default Footer;
