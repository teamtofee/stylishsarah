import React from "react";

const SocialMediaTab = ({ styleClasses, style, location, icon, name }) => {
  return (
    <div>
      <div className={styleClasses.tabWrapper}>
        <a href={location} className={styleClasses.tab} style={style.tab}>
          <span class={styleClasses.iconName} style={style.name}>
            {name}
          </span>
          <div class={styleClasses.icon} style={style.icon}>
            <img src={icon} />
          </div>
        </a>
      </div>
    </div>
  );
};

export default SocialMediaTab;
