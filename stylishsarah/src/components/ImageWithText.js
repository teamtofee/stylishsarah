import React from "react";
const src = "https://picsum.photos/500/300/?random";

const ImageWithText = ({ style, text, img }) => {
  return (
    <div>
      <div style={style.wrapper}>
        <div style={style.imgTextContainer}>
          <div style={style.textStyles.textContainer}>
            <div style={style.textStyles.textTitle}>{text.title}</div>
            <div style={style.textStyles.textSubtitle}>{text.subtitle}</div>
            <div style={style.textStyles.textBody}>{text.body}</div>
          </div>
          <div style={style.imgStyles.imgContainer}>
            <img style={style.imgStyles.img} src={img} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ImageWithText;
