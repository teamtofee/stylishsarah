import React from "react";
import styleClasses from "../styles/SocialMediaBar.module.css";
import SocialMediaTab from "./SocialMediaTab";

const SocialMediaBar = ({ icons }) => {
  return (
    <div className={styleClasses.container}>
      {icons.map((icon, index) => (
        <SocialMediaTab
          key={index}
          styleClasses={styleClasses}
          style={icon.style}
          location={icon.location}
          icon={icon.icon}
          name={icon.iconName}
        />
      ))}
    </div>
  );
};

export default SocialMediaBar;
