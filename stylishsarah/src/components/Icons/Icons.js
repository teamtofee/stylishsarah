import React from "react";
import styleClasses from "./styles.module.css";

const RemoveIcon = () => {
  return <a href="#" class={styleClasses.close} />;
};
export default RemoveIcon;
