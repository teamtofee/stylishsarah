import React, { Component } from "react";
import LandingHeroImage from "../images/home-process-step-3.jpg";

 class LandingHero extends Component {
   constructor(props){
     super(props)
     this.state ={
       screenwidth:window.innerWidth
     }
   }
   componentDidMount() {
     window.addEventListener("resize", this.resize.bind(this));
     this.resize();
   }
   resize() {
   this.setState({screenwidth: window.innerWidth});
 }
DesktopHero(){
  const {title,underBody,underTitle,bodyText,button,divBody,img,innerDiv} = styles
  return(
    <div style={divBody}>
    <div style={innerDiv}>

    </div>
    <div style={{width:"100%",padding:"4%"}}>
    <p style={title}>{this.props.title}</p>
    <p style={underTitle}>{this.props.underTitle}</p>
    <p style={bodyText}>{this.props.bodyText}</p>
    <p style={underBody}>{this.props.underBody}</p>
    <button className="button" >{this.props.buttonText}</button>
    </div>
    </div>
  )
}
mobileHero(){
  const {titleMobile,underBodyMobile,underTitleMobile,bodyTextMobile,buttonMobile,divBodyMobile,innerDivMobile} = styles
  return(
    <div style={divBodyMobile}>
    <div style={innerDivMobile}>
    <p style={titleMobile}>{this.props.title}</p>
    </div>
    <div style={{width:"100%",padding:"4%"}}>

    <p style={underTitleMobile}>{this.props.underTitle}</p>
    <p style={bodyTextMobile}>{this.props.bodyText}</p>
    <p style={underBodyMobile}>{this.props.underBody}</p>
    <button className="button" >{this.props.buttonText}</button>
    </div>
    </div>
  )
}
  render() {
    let DesktopHero = this.DesktopHero();
    let mobileHero = this.mobileHero();
    return(
      <div style={{marginTop:"30px"}}>
    {this.state.screenwidth>=800?DesktopHero:mobileHero}
    </div>
    )
  }
}
const styles={
  innerDiv:{
    backgroundImage:`url(${LandingHeroImage})`,
backgroundSize: "100%",
height:"auto",
width:"70%",
marginRight:"2%",
backgroundPosition: 'center',
 backgroundSize: 'cover',
 backgroundRepeat: 'no-repeat'
  },
divBody:{
  height: "auto",
  width: "100%",
  display:'flex',
  flexDirection: 'row',
  justifyContent:'center',
  backgroundColor:"#7768AE"
},
img:{
  height:"400px",
  marginRight:"40px"

},
title:{
  color: "#FFFFFF",
  fontFamily: "Bebas",
  fontSize: "50px",
  fontWeight: "600",
  marginBottom:"20px",
  letterSpacing: "0.59px",
  lineHeight: "50px"
},
underTitle:{
  color: "#FFFFFF",
  wordSpacing:"30px",
  fontFamily: "Open Sans",
  fontSize: "20px",
  marginBottom:"30px",
  fontWeight: "bold",
  letterSpacing: "0.3px",
  lineHeight: "27px"
},
bodyText:{
    color: "#FFFFFF",
    fontFamily: "Open Sans",
    fontSize: "18px",
      marginBottom:"20px",
    fontWeight: "100",
    lineHeight: "27px"
},
underBody:{
  color: "#FFFFFF",
  fontFamily: "Open Sans",
  fontSize: "18px",
    marginBottom:"40px",
  fontWeight: "bold",
  fontStyle:"italic",
  letterSpacing: "0.59px",
  lineHeight: "27px"
},


innerDivMobile:{
backgroundImage:`linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url(${LandingHeroImage})`,
backgroundSize: "100%",
height:"300px",
paddingTop:"5%",
paddingLeft:"5%",
paddingRight:"5%",
marginTop:"5%",
width:"100%",
backgroundPosition: 'center',
backgroundSize: 'cover',
marginRight:"5%",
backgroundRepeat: 'no-repeat'
},
divBodyMobile:{
height: "auto",
width: "100%",
display:'flex',
flexDirection: 'column',
backgroundColor:"#7768AE"
},
img:{
height:"400px",
marginRight:"40px"

},
titleMobile:{
color: "#FFFFFF",
fontFamily: "Bebas",
fontSize: "50px",
fontWeight: "600",
marginBottom:"20px",
letterSpacing: "0.59px",
lineHeight: "50px"
},
underTitleMobile:{
color: "#FFFFFF",
wordSpacing:"30px",
fontFamily: "Open Sans",
fontSize: "20px",
  letterSpacing: "0.3px",
  marginBottom:"30px",
fontWeight: "bold",
letterSpacing: "1.29px",
lineHeight: "27px"
},
bodyTextMobile:{
  color: "#FFFFFF",
  fontFamily: "Open Sans",
  fontSize: "18px",
    marginBottom:"20px",
  fontWeight: "100",
  lineHeight: "27px"
},
underBodyMobile:{
color: "#FFFFFF",
fontFamily: "Open Sans",
fontStyle:"italic",
fontSize: "18px",
  marginBottom:"40px",
fontWeight: "bold",
letterSpacing: "0.59px",
lineHeight: "27px"
}

}
export default LandingHero
