import React, { Component } from "react";
import { ReactTypeformEmbed } from 'react-typeform-embed';
import Styles from "../styles/Contact.css";



class Contact extends Component {
  constructor(props) {
     super(props);
     this.openForm = this.openForm.bind(this);
   }

   openForm() {
     this.typeformEmbed.typeform.open();
   }
  render() {
    return (
      <div className="ExamplePopup">
             <ReactTypeformEmbed
               popup
               autoOpen={false}
               url="https://dayanramirez.typeform.com/to/axibrP"
               hideHeaders
               hideFooter
               buttonText="Go!"
               style={{ top: 100 }}
               ref={tf => {
                 this.typeformEmbed = tf;
               }}
             />
           <button className="button" onClick={this.openForm} style={{ cursor: 'pointer' }}>
               Contact Me!
             </button>
           </div>


    );
}

}
export  default Contact;
