import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { NavHashLink } from "react-router-hash-link";

import logoBlack from "../logo_black.svg";
import logoWhite from "../logo_white.svg";
// Stlyes
import style from "../styles/Header.module.css";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNav: false,
      screenWidth: window.innerWidth
    };
    this.toggleNav = this.toggleNav.bind(this);
  }

  componentDidMount() {
    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
  }

  resize() {
    this.setState({ screenWidth: window.innerWidth });
  }

  toggleNav() {
    this.setState(prevState => {
      return {
        showNav: !prevState.showNav
      };
    });
  }
  render() {
    const mobileNav = (
      <div className={style.mobileNav}>
        <h1 className={style.mobileLogoText}>
          <img
            src={logoBlack}
            alt="StylishSarah"
            className={style.mobileLogoText__image}
          />
        </h1>
        <ul className={style.mobileNav__list}>
          <li className={style.mobileNav__item}>
            <NavLink
              to="/"
              className={style.mobileNav__link}
              onClick={this.toggleNav}
            >
              Home
            </NavLink>
          </li>
          <li className={style.mobileNav__item}>
            <NavLink
              to="/about"
              className={style.mobileNav__link}
              onClick={this.toggleNav}
            >
              About
            </NavLink>
          </li>
          <li className={style.mobileNav__item}>
            <NavHashLink
              smooth={true}
              to="/#the-process"
              className={style.mobileNav__link}
              onClick={this.toggleNav}
            >
              The Process
            </NavHashLink>
          </li>
          <li className={style.mobileNav__item}>
            <NavLink
              to="/testimonials"
              className={style.mobileNav__link}
              onClick={this.toggleNav}
            >
              Testimonials
            </NavLink>
          </li>
          <li className={style.navigation__item}>
            <NavHashLink
              smooth={true}
              to="/#eye-spy"
              className={style.navigation__link}
            >
              Eye Spy
            </NavHashLink>
          </li>
          <li className={style.mobileNav__item}>
            <NavLink
              to="/videos"
              className={style.mobileNav__link}
              onClick={this.toggleNav}
            >
              Videos
            </NavLink>
          </li>
          <li className={style.mobileNav__item}>
            <NavLink
              to="/fashionresume"
              className={style.mobileNav__link}
              onClick={this.toggleNav}
            >
              Fashion Resume
            </NavLink>
          </li>
          <li className={style.mobileNav__item}>
            <NavLink
              to="/contact"
              className={style.mobileNav__link}
              onClick={this.toggleNav}
            >
              Contact
            </NavLink>
          </li>
          <li className={style.mobileNav__item}>
            <NavLink
              to="/giftcards"
              className={style.mobileNav__link}
              onClick={this.toggleNav}
            >
              Gift Cards
            </NavLink>
          </li>
        </ul>
      </div>
    );

    return (
      <div>
        <div className={style.navigation}>
          <div className={style.navigation__button} onClick={this.toggleNav}>
            <div className={style.navigation__button__line} />
            <div className={style.navigation__button__line} />
            <div className={style.navigation__button__line} />
          </div>

          <div className={style.logo}>
            <img
              src={this.state.screenWidth >= 950 ? logoBlack : logoWhite}
              alt="StylishSarah"
              className={style.logo__image}
            />
          </div>

          <ul className={style.navigation__list}>
            <li className={style.navigation__item}>
              <NavLink to="/about" className={style.navigation__link}>
                About
              </NavLink>
            </li>
            <li className={style.navigation__item}>
              <NavHashLink
                smooth={true}
                to="/#the-process"
                className={style.navigation__link}
              >
                The Process
              </NavHashLink>
            </li>
            <li className={style.navigation__item}>
              <NavLink to="/testimonials" className={style.navigation__link}>
                Testimonials
              </NavLink>
            </li>
            <li className={style.navigation__item}>
              <NavHashLink
                smooth={true}
                to="/#eye-spy"
                className={style.navigation__link}
              >
                Eye Spy
              </NavHashLink>
            </li>
            <li className={style.navigation__item}>
              <NavLink to="/videos" className={style.navigation__link}>
                Videos
              </NavLink>
            </li>

            <li className={style.navigation__item}>
              <NavLink to="/fashionresume" className={style.navigation__link}>
                Fashion Resume
              </NavLink>
            </li>
            <li className={style.navigation__item}>
              <NavLink to="/contact" className={style.navigation__link}>
                Contact
              </NavLink>
            </li>
            <li className={style.navigation__item}>
              <NavLink to="/giftcards" className={style.navigation__link}>
                Gift Cards
              </NavLink>
            </li>
          </ul>
        </div>
        {this.state.showNav ? mobileNav : null}
      </div>
    );
  }
}

export default Header;
