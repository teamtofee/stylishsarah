import React, { Component } from 'react';
import sarah1 from "../images/home-process-step-1.jpg";
import background1 from "../images/background1.jpg";
import sarah2 from "../images/sarah2.png";
import styles from "../styles/about.js";

class About extends Component{
  constructor(props){
    super(props)
    this.state ={
      screenwidth:window.innerWidth
    }
  }
  componentDidMount() {
    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
  }
  resize() {
  this.setState({screenwidth: window.innerWidth});
}
renderMobile(){
  const {aboutHeroBackground,overallDiv,aboutHeroFontOne,aboutHeroFontTwo,innerDivImg,innerDivP,pStyle,aboutImg,aboutInfo,aboutParagraph, infoHeader} = styles;
}
renderDesktop(){
  const {aboutHeroBackground,midStyleEnd,overallDiv,midStyle,aboutHeroFontOne,aboutHeroFontTwo,innerDivImg,innerDivP,pStyle,aboutImg,aboutInfo,aboutParagraph, infoHeader} = styles;
  return(
    <div>
      <div style={aboutHeroBackground}>
      <p style={pStyle}>
      <span style={aboutHeroFontOne}>Sarah Sulzberger-Perpich</span><br></br><br></br>
      <span style={aboutHeroFontTwo}>For women, shopping for something as simple as a classic t-shirt can be overwhelming and frustrating. Whether you’re at Saks or the Gap, the options are endless, and it’s easy to get lost. That’s where I come in: Get ready to get a little more stylish!</span>

      </p>
      </div>
              <h1 style={infoHeader}>ABOUT ME</h1>
      <div style={overallDiv}>
        <div  style={aboutInfo}>

          <div style={innerDivImg}><img src={sarah1} style={aboutImg}></img></div>
          <div style={innerDivP}><p style={aboutParagraph}>This year marks my 21st year working in the various aspects of the multi-faceted fashion industry. While I have worked for many recognized designers and fashion publications, I continue to come back to my passion for personal styling. I began my career interning as a writer and stylist with former fashion editor, Amy Spindler, and former Fashion Director,  Anne Christensen. My experience at the New York Times Fashion Magazine (Fashion Of The Times) kick-started <span style={{'color':'black','fontWeight':'bold'}}>my love affair, passion, and dedication to a lifelong career in the fashion world.</span>
</p></div>
        </div>
        <h1 style={midStyle}>MY PASSION</h1>
        <div  style={aboutInfo}>
          <div style={innerDivP}><p style={aboutParagraph}>I have worked in fashion editorial on and off, writing, and styling models and celebrities for publications such as The New York Times, WWD, Salon News, V Magazine, and Style.com (among many more).But, after working for four years as a personal shopper for Bloomingdales and Henri Bendel, I realized that my <span style={{'color':'black','fontWeight':'bold'}}>true passion lies with real women on the sidewalks</span>, not models on the pages of magazines. Wanting to fulfill my clients’ needs from a 360-degree approach, in 2009, I made the decision to branch out on my own – thus, the birth of StylishSarah – my own personal styling and fashion consultancy.</p></div>
          <div style={innerDivImg}><img src={sarah2} style={aboutImg}></img></div>
        </div>
        <h1 style={midStyle}>MY DEDICATION</h1>
        <div  style={aboutInfo}>

          <div style={innerDivImg}><img src={sarah2} style={aboutImg}></img></div>
          <div style={innerDivP}><p style={aboutParagraph}>I’ve been lucky enough to work in-house with designers like Zac Posen and with renowned editors such as Dirk Standon of Style.com, Sophia Chabbot and Arthur Friedman of WWD, and photographer Bill Cunningham. I am a proud member of both The Couture Council of the Museum at FIT and All Walks Beyond the Catwalk. You can easily be found me on the Fashion Police page in UsWeekly magazine. <span style={{'color':'black','fontWeight':'bold'}}>These valuable experiences allowed me to cultivate the comprehensive skill set that I now apply to personal styling.</span></p></div>
        </div>
        <br></br>
        <br></br>
        <h1 style={midStyle}>YOUR FASHION</h1>
        <h1 style={midStyleEnd}>Your Life. Styled.</h1>
      </div>
    </div>
  )

}
  render(){
    let desktop= this.renderDesktop();
    let mobile = this.renderMobile();
    return(
      <div>
        {this.state.screenwidth>=800?desktop:mobile}
      </div>
    )
  }
}

export default About
