import React from "react";
import ImageWithText from "./ImageWithText";
import styles from "../styles/Process";
import img1 from "../images/home-process-step-1.jpg";
import img2 from "../images/home-process-step-2.jpg";
import img3 from "../images/home-process-step-3.jpg";
import img4 from "../images/home-process-step-4.jpg";

//Process Component
const Process = () => {
  const content = getContent();
  return (
    <div style={styles.container}>
      <div id={"the-process"} style={styles.theProcess}>
        THE PROCESS
      </div>
      {content.map((data, index) => (
        <ImageWithText
          key={index}
          style={data.styles}
          text={data.text}
          img={data.img}
        />
      ))}
    </div>
  );
};

const getContent = () => {
  return [
    {
      img: img1,
      text: {
        title: "01",
        subtitle: "Getting To Know You",
        body:
          "What is a day in your life like? This process is all about YOU. We’ll determine your fashion goals by learning about who you are by finding out about your lifeSTYLE. I take into consideration your daily routines, current career, environment, hobbies, and interests."
      },
      styles: {
        textStyles: {
          ...styles.textStyles,
          textTitle: { ...styles.textStyles.textTitle, marginBottom: "0px" }
        },
        imgStyles: styles.process_1.imgStyles,
        wrapper: styles.wrapper,
        imgTextContainer: styles.imgTextContainer
      }
    },
    {
      img: img2,
      text: {
        title: "02",
        subtitle: "Closet Consultation",
        body:
          "The most important part about an item of clothing is FIT. You will try on everything in your closet! Yes, everything. This is where you'll learn HOW your clothes should fit – tucking, plucking, lifting, raising and hemming. You will learn the best styles and fit for your height, and body type."
      },
      styles: {
        textStyles: {
          ...styles.textStyles,
          textContainer: { ...styles.textStyles.textContainer, float: "right" },
          textTitle: { ...styles.textStyles.textTitle, float: "right" },
          textSubtitle: { ...styles.textStyles.textSubtitle, clear: "right" },
          textBody: {
            ...styles.textStyles.textBody,
            width: "337px",
            height: "239px"
          }
        },
        imgStyles: styles.process_2.imgStyles,
        wrapper: styles.wrapper,
        imgTextContainer: styles.imgTextContainer
      }
    },
    {
      img: img3,
      text: {
        title: "03",
        subtitle: "Shopping Trips",
        body:
          "Let’s get this styling extravaganza started! Time to hit the stores and get everything on the list I think you’ve missed. From a staple sheath dress and slacks to your belt, bangles, booties, and bags. We’ll find you quality, versatile, seasonless wardrobe staples that will last a lifetime."
      },
      styles: {
        textStyles: {
          ...styles.textStyles,
          textBody: {
            ...styles.textStyles.textBody,
            width: "332px",
            height: "265px"
          }
        },
        imgStyles: styles.process_3.imgStyles,
        wrapper: styles.wrapper,
        imgTextContainer: styles.imgTextContainer
      }
    },
    {
      img: img4,
      text: {
        title: "04",
        subtitle: "Styling Recap Session",
        body:
          "Together we go through all of the new and old pieces from your wardrobe and experiment with all of the various looks you can create. You'll learn how to mix and match your new pieces with your older staples, what to accessorize and accentuate, and how to determine what looks will easily go from day to evening in order to get the most mileage out of each piece."
      },
      styles: {
        textStyles: {
          ...styles.textStyles,
          textContainer: { ...styles.textStyles.textContainer, float: "right" },
          textTitle: {
            ...styles.textStyles.textTitle,
            float: "right",
            marginBottom: "0px"
          },
          textSubtitle: {
            ...styles.textStyles.textSubtitle,
            clear: "right",
            width: "220px"
          },
          textBody: {
            ...styles.textStyles.textBody,
            width: "301px",
            height: "354px"
          }
        },
        imgStyles: styles.process_4.imgStyles,
        wrapper: styles.wrapper,
        imgTextContainer: styles.imgTextContainer
      }
    }
  ];
};

export default Process;
