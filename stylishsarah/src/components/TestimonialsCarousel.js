import React, { Component } from "react";
import { Button,Carousel } from 'react-bootstrap';
import styles from "../styles/TestimonialsCarousel";
import "../styles/testimonialcarousel.css";







class TestimonialsCarousel extends Component {

  render() {
    return (

      <div style={styles.container} className="container">
        <h1 style={styles.heading} >WHAT CLIENTS SAY </h1>
      <Carousel style={styles.carouselContainer} className="carouselContainer" >
  <Carousel.Item >
  <p style={styles.paragraph} className="text" >"I had an amazing time shopping with Sarah. Usually, I feel so overwhelmed shopping and end up buying items I don’t necessarily need. Sarah was so helpful during the closet clean-out."</p>

            <p style={styles.name} >-Jamie</p>
  </Carousel.Item>


  <Carousel.Item >
  <p style={styles.paragraph} className="text"  >"Since working with Sarah, I’ve gotten a TON of compliments on how pulled-together and chic I look and it’s been fairly effortless. I don’t actually spend much more time getting ready, but I have found I instinctively have a better idea of what works on me and what works together."</p>
    <p style={styles.name} >-Lindsey</p>
  </Carousel.Item>


  <Carousel.Item >
  <p style={styles.paragraph} className="text"  >"Sarah helped me get out of the rut I had fallen into after becoming a mom of toddler twins. She helped me edit and organize my wardrobe with most exquisite basics Six, I repeat, six gigantic bags of frumpy mom gear were carted out of my apartment!"</p>
       <p style={styles.name} >-Noreen</p>
  </Carousel.Item>


  <Carousel.Item >
  <p style={styles.paragraph} className="text"  >"Needless to say I was the best dressed at the event! She also took me shopping and what I normally find to be a frustrating process, Sarah made it fun and stress free. I got some season-less stylish basics that I can wear for many years. "</p>
    <p style={styles.name} >-Stephanie</p>
  </Carousel.Item>



  <Carousel.Item >
  <p style={styles.paragraph} className="text"  >" I did not have basics in my closet-the little black dress I could run out in-Sarah helped me step it up a notch. And those things were things I would not have though of investing in . I was looking forward to Sarah curating and customizing my closet for me."</p>
    <p style={styles.name} >-Shilpa</p>
  </Carousel.Item>



</Carousel>

<button className="button1" type="button">Read More!</button>

      </div>

    );
  }
}

export default TestimonialsCarousel;
