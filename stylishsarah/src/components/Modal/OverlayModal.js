import React from "react";
import PropTypes from "prop-types";
// import CssModules from "react-css-modules";
import styleClasses from "./OverlayModal.scss";

import Modal from "./Modal";
import classes from "./OverlayModal.scss";

export default class OverlayModal extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    handleClose: PropTypes.func.isRequired
  };

  render() {
    return (
      <Modal>
        <div styleName="wrapper">
          <div styleName="inner">
            <button onClick={this.props.handleClose} styleName="close">
              <a href="#" class={styleClasses.close} />
            </button>
            {this.props.children}
          </div>
        </div>
      </Modal>
    );
  }
}
