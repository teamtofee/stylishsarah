import React, { Component } from "react";
import styles from "../images/home-process-step-3.jpg";


 class Eyespy extends Component {
constructor(props){
  super(props)
this.state = {dimensions: {}};
this.onImgLoad = this.onImgLoad.bind(this);
}
onImgLoad({target:img}) {
    this.setState({dimensions:{height:img.offsetHeight,
    width:img.offsetWidth}});
}
renderBoxes(){
  const divList=[];
  for(var i = 0; i < 15;i++){
    divList.push(
      <div className={`item${i+1}`}>
      </div>
    )
  }
  return divList
}

render(){
  const {width, height} = this.state.dimensions;
  let renderBoxes = this.renderBoxes();
return (<div>
      <p className="eyespyHeader">#EYE SPY</p>
        <div className="container">
          {renderBoxes}
        </div>
        </div>
       );
     }
}

export default Eyespy
