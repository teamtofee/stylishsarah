import React, { Component } from "react";
import LandingHero from "./LandingHero";
import Process from "./Process";
import icon1 from "../images/social-facebook - simple-line-icons.png";
import icon2 from "../images/instagram - FontAwesome.png";
import icon3 from "../images/social-twitter - simple-line-icons.png";
import icon4 from "../images/social-youtube - simple-line-icons.png";
import SocialMediaBar from "./SocialMediaBar";
import TestimonialsCarousel from "./TestimonialsCarousel";
const icons = [
  {
    icon: icon1,
    iconName: "FaceBook",
    location: "www.facebook.com",
    style: {}
  },
  {
    icon: icon2,
    iconName: "Instagram",
    location: "www.Instagram.com",
    style: {}
  },
  {
    icon: icon3,
    iconName: "Twitter",
    location: "www.Twitter.com",
    style: {}
  },
  {
    icon: icon4,
    iconName: "Youtube",
    location: "www.Youtube.com",
    style: {}
  }
];

export default class LandingPage extends Component {
  render() {
    return (
      <div>
        <div style={{ position: "relative", top: "250px" }}>
          <SocialMediaBar icons={icons} />
        </div>

        <LandingHero
          bodyText="I believe the best style is Personal Style. You have it. I'm here to show, teach, learn and listen so I can pull it out of you! My clients range from 15-75 sizes 00 to 14. I can find the most fabulous, flattering fabrics, cuts, and styles that best fit your lifestyle, body and personality."
          underBody=" Signature statements that celebrate YOU!"
          title="Your 	LIFE styled."
          underTitle="Myself.		Work.		Skill. "
          buttonText="Get to Know Me"
        />
        <Process />
        <TestimonialsCarousel />
      </div>
    );
  }
}
