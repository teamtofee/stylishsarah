const styles = {
  container:{
     background: "#F2F0F3",
     width:"100%",

  },
  heading:{
    fontSize: "3vh",
    textAlign: "center",
    paddingTop:"10px",
    letterSpacing:"5px",
    fontFamily:"open sans",
    fontWeight:"600",
    textDecoration:"none",
    color:"#333",
  },
  a:{
    textDecoration:"none",
    color:"#333",
  },
  carouselContainer:{
     background: "#F2F0F3",
     width:"100%",
     padding:"4% 8%",

  },
  paragraph:{
  fontFamily: "Open Sans",
  fontStyle: "italic",
  fontWeight: "normal",
  lineHeight: "30px",
  fontSize: "19px",
  textAlign: "center",
  letterSpacing: "1.33864px",
paddingRight:"8%",
paddingLeft:"8%",
paddingBottom:"0%",


  color: "#000000",
},
name:{
  fontFamily:"open sans",
  fontWeight:"light",
  fontSize: "2.5vh",
  textAlign: "center",
  color: "black",
  paddingTop:"10px",
  paddingBottom:"20px",
  color:"#333",
},
btn:{
    background:"#A195CD",
    fontFamily: "Open Sans",
  	fontSize: "15px",
  	fontWight: "bold",
  	letterSpacing: "0.5px",
    float:"right",
    padding:"10px",
    display:"block",
    borderRadius: "0px",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 75)",
  }

};
export default styles; 
