import "./index.css";

const styles = {
  container: {
    width: "100%"
  },
  wrapper: {
    width: "100%",
    textAlign: "center",
    marginBottom: "30px"
  },
  imgTextContainer: { width: "880px", display: "inline-block" },
  theProcess: {
    margin: "auto auto 120px",
    height: "62px",
    width: "264px",
    color: "#000000",
    fontFamily: "Bebas",
    fontSize: "47px",
    letterSpacing: "1.18px",
    lineHeight: "62px"
  },
  textStyles: {
    textContainer: { float: "left" },
    textTitle: {
      height: "63px",
      width: "45px",
      color: "#a195cd",
      fontFamily: "Bebas",
      fontSize: "48px",
      letterSpacing: "1.2px",
      lineHeight: "63px",
      marginBottom: "20px"
    },
    textSubtitle: {
      height: "27px",
      width: "215px",
      color: "rgba(0, 0, 0, 0.85)",
      fontFamily: "Open Sans",
      fontSize: "20px",
      fontWeight: "bold",
      letterSpacing: "0.5px",
      lineHeight: "27px",
      textAlign: "left",
      padding: "25px 0 30px 0"
    },
    textBody: {
      height: "194px",
      width: "394px",
      color: "rgba(0, 0, 0, 0.75)",
      fontFamily: "Open Sans",
      fontSize: "18px",
      letterSpacing: "0.45px",
      lineHeight: "30px",
      textAlign: "left"
    }
  },
  process_1: {
    imgStyles: {
      imgContainer: {
        height: "326px",
        width: "450px",
        boxShadow: "0 6px 6px 0 rgba(0, 0, 0, 0.5)",
        float: "right",
        textAlign: "center",
        overflow: "hidden",
        marginLeft: "25px",
        marginTop: "70px"
      },
      img: {
        marginLeft: "-1%",
        objectFit: "fill"
      }
    }
  },
  process_2: {
    imgStyles: {
      imgContainer: {
        height: "341px",
        width: "500px",
        boxShadow: "0 6px 6px 0 rgba(0, 0, 0, 0.5)",
        float: "left",
        textAlign: "center",
        overflow: "hidden",
        marginTop: "40px"
      },
      img: {
        objectFit: "fill"
      }
    }
  },
  process_3: {
    imgStyles: {
      imgContainer: {
        height: "271px",
        width: "417px",
        boxShadow: "0 6px 6px 0 rgba(0, 0, 0, 0.5)",
        float: "right",
        textAlign: "center",
        overflow: "hidden",
        marginLeft: "25px",
        marginTop: "60px"
      },
      img: {
        height: "100%",
        width: "100%",
        objectFit: "cover"
      }
    }
  },
  process_4: {
    imgStyles: {
      imgContainer: {
        height: "293px",
        width: "506px",
        boxShadow: "0 6px 6px 0 rgba(0, 0, 0, 0.5)",
        float: "left",
        textAlign: "center",
        overflow: "hidden",
        marginRight: "-25px",
        marginTop: "100px"
      },
      img: {
        height: "100%",
        width: "100%",
        objectFit: "cover"
      }
    }
  }
};

export default styles;
