import background1 from "../images/background1.jpg"
const styles = {
  aboutHeroBackground:{
backgroundImage:`linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url(${background1})`,
    paddingTop:'15%',
    paddingLeft:'8%',
    backgroundSize: "100%",
    height:"700px",
    width:"100%",
    marginRight:"2%",
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
  },
  innerDivImg:{
    width: "45%",
    height:"35%",
    margin:'0',
    padding:'0'
  },
  midStyle:{
    fontStyle:'italic',
    textAlign:'center',
    letterSpacing:'7px',
    fontSize:'19px'
  },
  midStyleEnd:{

    textAlign:'center',

    fontSize:'19px'
  },
  innerDivP:{
    width: "50%",
    height:"35%",
    margin:'0',
    padding:'0'
  },
  aboutHeroFontOne:{
      color: '#FFFFFF',
      fontFamily: "Open Sans",
      fontSize: '40px',
      fontWeight:'900'
  },
  aboutImg:{
    width:'100%',
    height:'100%',
    margin:'0'
  },
  aboutParagraph:{
    textAlign:'left',
    paddingLeft:'10%',
    paddingRight:'10%',
    fontWeight:"100",
    	color: 'rgba(0,0,0,0.75)',
      fontFamily: "Open Sans",
      fontSize: '18px',
      lineHeight: '30px'
  },
  overallDiv:{
    marginBottom:'10%',
    marginTop:'5%'
  },
  aboutInfo:{
    width:'100%',
    height:'auto',
    marginBottom:"6%",
    paddingTop:'5%',
    paddingLeft:"5%",
    display:'flex',
    flexDirection:'row',
    flexWrap:'wrap'
  },
  infoHeader:{
    color: '#000000',
    fontFamily: "Open Sans",
    fontSize: '30px',
    textAlign:'center',
    fontWeight: 400,
    marginTop:"10%",
    fontWeight:"900",
    marginBottom:'50px',
    letterSpacing: '10px',
    lineHeight: '64px'
  },
  aboutHeroFontTwo:{
    color: '#FFFFFF',
    fontFamily: "Open Sans",
    fontSize: '20px',
    fontWeight: '600'
  },
  pStyle:{
    float:'left',
    lineHeight:'1cm',
    width:'50%'
  }
};
export default styles
