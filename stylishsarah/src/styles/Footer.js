const styles = {
  container: {
    listStyleType: "none",
    position: "relative",
    float: "left",
    top: "110px",
    bottom: "256px",
    width: "33%",
    textDecoration: "none"
  },
  footer: {
    height: "407px",
    width: "100%",
    background: "#a195cd"
  },
  stylishSarah: {
    height: "40px",
    width: "220px",
    color: "#ffffff",
    fontFamily: "Valencia-Serial-Heavy",
    fontSize: "36px",
    letterSpacing: "-05px",
    lineHeight: "40px",
    fontWeight: "bolder",
    position: "relative",
    left: "129px"
  },
  work: {
    height: "27px",
    width: "61px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "20px",
    fontWeight: "600",
    lineHeight: "27px",
    position: "relative",
    top: "10px",
    left: "92px"
  },
  followMe: {
    height: "27px",
    width: "61px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "20px",
    fontWeight: "600",
    lineHeight: "27px",
    position: "relative",
    top: "10px"
  },
  fashionResume: {
    height: "24px",
    width: "138px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "24px",
    position: "relative",
    top: "26px",
    left: "92px"
  },
  contact: {
    height: "24px",
    width: "56px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "40px",
    position: "relative",
    top: "26px",
    left: "92px"
  },
  giftcard: {
    height: "24px",
    width: "56px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "40px",
    position: "relative",
    top: "26px",
    left: "92px"
  },
  videos: {
    height: "24px",
    width: "56px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "40px",
    position: "relative",
    top: "26px",
    left: "92px"
  },
  theProcess: {
    height: "24px",
    width: "101px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "24px",
    marginTop: "22px",
    position: "relative",
    top: "10px",
    left: "164px"
  },
  about: {
    height: "24px",
    width: "51px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "40px",
    position: "relative",
    top: "10px",
    left: "164px"
  },
  line1: {
    boxSizing: "border-box",
    height: "1px",
    width: "310px",
    border: "2px solid #cdc5e8",
    position: "relative",
    top: "-2px",
    left: "163px"
  },
  line2: {
    boxSizing: "border-box",
    height: "1px",
    width: "310px",
    border: "2px solid #cdc5e8",
    position: "relative",
    top: "10px",
    left: "88px"
  },
  line3: {
    boxSizing: "border-box",
    height: "1px",
    width: "340px",
    border: "2px solid #cdc5e8",
    top: "10px",
    position: "relative"
  },

  eyespy: {
    height: "24px",
    width: "64px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "40px",
    position: "relative",
    top: "10px",
    left: "164px"
  },
  facebook: {
    height: "24px",
    width: "81px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "24px",
    position: "relative",
    top: "26px"
  },
  twitter: {
    height: "24px",
    width: "59px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "40px",
    position: "relative",
    top: "26px"
  },
  instagram: {
    height: "24px",
    width: "85px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "40px",
    position: "relative",
    top: "26px"
  },
  youtube: {
    height: "24px",
    width: "85px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "18px",
    lineHeight: "40px",
    position: "relative",
    top: "26px"
  },
  stylishSarahSara: {
    width: "889px",
    color: "#ffffff",
    fontFamily: "Open Sans",
    fontSize: "20px",
    letterSpacing: "103px",
    position: "relative",
    left: "276px",
    top: "1735px"
  }
};
export default styles;
